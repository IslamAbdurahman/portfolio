@extends('.layouts.layout')

@section('main')
<main id="main">

    <!-- ======= About Section ======= -->
    <div id="about" class="paddsection">
        <div class="container">
            <div class="row justify-content-between">

                <div class="col-lg-4 ">
                    <div class="div-img-bg">
                        <div class="about-img">
                            <img src="{{ asset('public/assets/img/017.jpg') }}" class="img-responsive" alt="me">
                        </div>
                    </div>
                </div>

                <div class="col-lg-7">
                    <div class="about-descr">

                        <p class="p-heading"></p>
                        <h3>Download my CV <a class="text-primary text-decoration-underline" href="{{ asset('public/assets/img/Islombek_Abdurakhmonov_CV.pdf') }}" target="_blank">HERE</a></h3>
                        <p>I've been developing, architecting, and leading software projects for well over a year.
                            I usually write web services and APIs used by front-end developers and mobile application developers.</p>
                        <p class="separator"><ul>
                            <li>Managing the interchange of data between the server and the users.</li>
                            <li>Integration of user-facing elements developed by a front-end developers with server side logic</li>
                            <li>Building reusable code and libraries for future use</li>
                            <li>Optimization of the application for maximum speed and scalability</li>
                            <li>Implementation of security and data protection</li>
                            <li>Design and implementation of data storage solutions</li>
                        </ul></p>

                    </div>

                </div>
            </div>
        </div>
    </div><!-- End About Section -->

    <!-- ======= Services Section ======= -->
    <div id="services">
        <div class="container">

            <div class="services-slider swiper" data-aos="fade-up" data-aos-delay="100">
                <div class="swiper-wrapper">

                    <div class="swiper-slide">
                        <div class="services-block">
                            <i class="bi bi-briefcase"></i>
                            <span>Back End Developer</span>
                            <p class="separator">Php, Laravel, Mysql, Postgresql, Telegram Bot</p>
                        </div>
                    </div><!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="services-block">
                            <i class="bi bi-card-checklist"></i>
                            <span>Front End Developer</span>
                            <p class="separator">Html, Css, Bootstrap, Sass, Scss, Javascript </p>
                        </div>
                    </div><!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="services-block">
                            <i class="bi bi-bar-chart"></i>
                            <span>WEB Developer</span>
                            <p class="separator">Html, Css, Bootstrap, Sass, Scss, Javascript, Php, Laravel, Mysql</p>
                        </div>
                    </div><!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="services-block">
                            <i class="bi bi-binoculars"></i>
                            <span>MOBILE APPS</span>
                            <p class="separator">Php, Laravel, Mysql, MongoDB, MariaDB, Postgresql, API, Database</p>
                        </div>
                    </div><!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="services-block">
                            <i class="bi bi-brightness-high"></i>
                            <span>Analytics</span>
                            <p class="separator"> Web servers</p>
                        </div>
                    </div><!-- End testimonial item -->


                </div>
                <div class="swiper-pagination"></div>
            </div>

        </div>

    </div><!-- End Services Section -->

    <!-- ======= Portfolio Section ======= -->
    <div id="portfolio" class="paddsection">

        <div class="container">
            <div class="section-title text-center">
                <h2>My Portfolio</h2>
            </div>
        </div>

        <div class="container">

            <div class="row">
                <div class="col-lg-12 d-flex justify-content-center">
                    <ul id="portfolio-flters">
                        <li data-filter="*" class="filter-active">All</li>
                        <li data-filter=".filter-app">App</li>
                        <li data-filter=".filter-web">Web</li>
                    </ul>
                </div>
            </div>

            <div class="row portfolio-container">

                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                    <img src="{{ asset('public/assets/img/portfolio/rio-app.jpg') }}" class="img-fluidc" alt="" height="200px">
                    <div class="portfolio-info">
                        <h4>Rio Delivery</h4>
                        <p>Rio Delivery</p>
                        <a href="{{ asset('public/assets/img/portfolio/rio-app.jpg') }}" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Rio Delivery"><i class="bx bx-plus"></i></a>
                        <a href="" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                    <img src="{{ asset('public/assets/img/portfolio/apple-app.jpg') }}" class="img-fluidc" alt="" height="200px">
                    <div class="portfolio-info">
                        <h4>Apple Optom Tashkent</h4>
                        <p>Apple Optom Tashkent</p>
                        <a href="{{ asset('public/assets/img/portfolio/apple-app.jpg') }}" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Apple Optom Tashkent"><i class="bx bx-plus"></i></a>
                        <a href="" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                    <img src="{{ asset('public/assets/img/portfolio/Apple.uz.jpg') }}" class="img-fluidc " alt="" height="200px">
                    <div class="portfolio-info">
                        <h4>Web Admin Panels</h4>
                        <p>Web Admin Panels</p>
                        <a href="{{ asset('public/assets/img/portfolio/Apple.uz.jpg') }}" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Web Admin Panels"><i class="bx bx-plus"></i></a>
                        <a href="" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                    <img src="{{ asset('public/assets/img/portfolio/riodelivery.jpg') }}" class="img-fluidc" alt="" height="200px">
                    <div class="portfolio-info">
                        <h4>Web Admin Panels</h4>
                        <p>Web Admin Panels</p>
                        <a href="{{ asset('public/assets/img/portfolio/riodelivery.jpg') }}" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Web Admin Panels"><i class="bx bx-plus"></i></a>
                        <a href="" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                    <img src="{{ asset('public/assets/img/desktop.jpg') }}" class="img-fluidc" alt="" height="200px">
                    <div class="portfolio-info">
                        <h4>Smarttrade Desktop</h4>
                        <p>Smarttrade Desktop</p>
                        <a href="{{ asset('public/assets/img/desktop.jpg') }}" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Smarttrade"><i class="bx bx-plus"></i></a>
                        <a href="" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                    <img src="{{ asset('public/assets/img/smarttrade.jpg') }}" class="img-fluidc" alt="" height="200px">
                    <div class="portfolio-info">
                        <h4>Smarttrade App</h4>
                        <p>Smarttrade App</p>
                        <a href="{{ asset('public/assets/img/smarttrade.jpg') }}" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Smarttrade"><i class="bx bx-plus"></i></a>
                        <a href="" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                    <img src="{{ asset('public/assets/img/savdo_oyna.JPG') }}" class="img-fluidc" alt="" height="200px">
                    <div class="portfolio-info">
                        <h4>Smarttrade App Savdo Oyna</h4>
                        <p>Smarttrade App Savdo oyna</p>
                        <a href="{{ asset('public/assets/img/savdo_oyna.JPG') }}" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Savdo oyna"><i class="bx bx-plus"></i></a>
                        <a href="" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
                    </div>
                </div>


            </div>

        </div>

    </div><!-- End Portfolio Section -->

    <!-- ======= Journal Section ======= -->
    <div id="journal" class="text-left paddsection">

        <div class="container">
            <div class="section-title text-center">
                <h2>journal</h2>
            </div>
        </div>

        <div class="container">
            <div class="journal-block">
                <div class="row">

                    <div class="col-lg-4 col-md-6">
                        <div class="journal-info">

                            <a href=""><img src="{{ asset('public/assets/img/blog-post-1.jpg') }}" class="img-responsive" alt="img"></a>

                            <div class="journal-txt">

                                <h4><a href="">SO LETS MAKE THE MOST IS BEAUTIFUL</a></h4>
                                </p>

                            </div>

                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="journal-info">

                            <a href=""><img src="{{ asset('public/assets/img/blog-post-2.jpg') }}" class="img-responsive" alt="img"></a>

                            <div class="journal-txt">

                                <h4><a href="">WE'RE GONA MAKE DREAMS COMES</a></h4>
                                </p>

                            </div>

                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="journal-info">

                            <a href=""><img src="{{ asset('public/assets/img/blog-post-3.jpg') }}" class="img-responsive" alt="img"></a>

                            <div class="journal-txt">

                                <h4><a href="">NEW LIFE CIVILIZATIONS TO BOLDLY</a></h4>
                                </p>

                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div><!-- End Journal Section -->

    <!-- ======= Contact Section ======= -->
    <div id="contact" class="paddsection">
        <div class="container">
            <div class="contact-block1">
                <div class="row">

                    <div class="col-lg-6">
                        <div class="contact-contact">

                            <h2 class="mb-30">GET IN TOUCH</h2>

                            <ul class="contact-details">
                                <li><span>33 Railway, Azimobod, Altyaryk</span></li>
                                <li><span>150100, FERGANA, Uzbekistan</span></li>
                                <li><span><a href="+998 911157709">+998 911157709</a></span></li>
                                <li><span>onlymarch567@gmail.com</span></li>
                            </ul>

                        </div>
                    </div>

                    <div class="col-lg-6">
                        <form action="{{ route('message') }}" method="post" role="form" class="php-email-form">
{{--                            <form action="{{ route('message') }}" method="post" role="form" class="php-email-form">--}}
                            @csrf
                            <div class="row gy-3">

                                <div class="col-lg-6">
                                    <div class="form-group contact-block1">
                                        <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" required>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" required>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" required>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone" required>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <textarea class="form-control" name="message" placeholder="Message" required></textarea>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="loading">Loading</div>
                                    <div class="error-message"></div>
                                    <div class="sent-message">Your message has been sent. Thank you!</div>
                                </div>

                                <div class="mt-0">
                                    <input type="submit" class="btn btn-defeault btn-send" value="Send message">
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- End Contact Section -->

</main><!-- End #main -->

@endsection
